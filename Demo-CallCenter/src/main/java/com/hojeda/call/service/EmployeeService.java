package com.hojeda.call.service;

import java.util.List;

import com.hojeda.call.model.Employee;
import com.hojeda.call.model.Employee.State;
import com.hojeda.call.service.generic.GenericService;

public interface EmployeeService extends GenericService<Employee, Long>{

	List<Employee> findByState(State state);
}
