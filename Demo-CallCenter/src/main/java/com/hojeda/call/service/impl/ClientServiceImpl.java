package com.hojeda.call.service.impl;

import org.springframework.stereotype.Service;

import com.hojeda.call.dao.ClientRepository;
import com.hojeda.call.model.Client;
import com.hojeda.call.service.ClientService;
import com.hojeda.call.service.generic.GenericAbstractService;

@Service
public class ClientServiceImpl extends GenericAbstractService<ClientRepository, Client, Long> implements ClientService {

}
