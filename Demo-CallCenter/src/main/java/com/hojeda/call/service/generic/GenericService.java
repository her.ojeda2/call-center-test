package com.hojeda.call.service.generic;

import java.util.List;
import java.util.Optional;

public interface GenericService<T, ID> {

	T save(T object);
	T update(T object);
	void remove(T object);
	List<T> findAll();
	Optional<T> findById(ID id);
	boolean exists(ID id);
	List<T> saveAll(List<T> entities);
	List<T> findAllLimitResult(Integer limitSize);
	Long count();
}
