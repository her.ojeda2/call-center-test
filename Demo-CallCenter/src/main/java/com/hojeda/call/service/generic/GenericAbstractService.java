package com.hojeda.call.service.generic;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * <h1> Servicio para entidades genérico </h1>
 * <br>
 * Este servicio provee una serie de métodos reutilizables para todas las entidades de JPA.
 * <br>
 * A su vez también infiere el repositorio de los parámetros genéricos.
 * 
 * */
public abstract class GenericAbstractService <REPO extends JpaRepository<T, ID>,T, ID extends Serializable> implements GenericService<T, ID>{

	@Autowired
	private REPO repo;
	
	@Override
	@Transactional(readOnly = true)
	public Optional<T> findById(ID id) {
		return Optional.ofNullable( repo.findOne(id));
	}
	
	@Override
	@Transactional
	public T save(T entity) {
		return repo.save(entity);
	}
	
	@Override
	@Transactional
	public void remove(T entity) {
		repo.delete(entity);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<T> findAll() {
		return repo.findAll();
	}
	
	@Override
	@Transactional
	public T update(T entity) {
		return repo.save(entity);
	}
	
	@Transactional
	public List<T> saveAll(List<T> list) {
		return (List<T>) repo.save(list);
	}
	
	@Transactional(readOnly=true)
	public List<T> findAllLimitResult(Integer limitSize) {
		Page<T> resultPage = getRepository().findAll(new PageRequest(0, limitSize));
		return resultPage.getContent();
	}

	@Override
	public boolean exists(ID id) {
		return repo.exists(id);
	}
	
	@Override
	public Long count() {
		return repo.count();
	}
	
	protected REPO getRepository() {
		return repo;
	}
}
