package com.hojeda.call.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hojeda.call.dao.CallRepository;
import com.hojeda.call.model.Call;
import com.hojeda.call.model.Call.State;
import com.hojeda.call.service.CallService;
import com.hojeda.call.service.generic.GenericAbstractService;

@Service
public class CallServiceImpl extends GenericAbstractService<CallRepository, Call, Long> implements CallService{

	@Transactional(readOnly=true)
	public Boolean existCallByState(State state) {
		return getRepository().countCallByState(state) > 0;
	}
	
	@Transactional(readOnly=true)
	public List<Call> findByState(State state) {
		return getRepository().findByState(state);
	}
	
	@Transactional
	public void truncateCalls() {
		getRepository().truncate();
	}

}
