package com.hojeda.call.service;

import java.util.List;

import com.hojeda.call.model.Call;
import com.hojeda.call.model.Call.State;
import com.hojeda.call.service.generic.GenericService;

public interface CallService extends GenericService<Call, Long>{

	Boolean existCallByState(Call.State state);
	List<Call> findByState(State state);
	void truncateCalls();
}
