package com.hojeda.call.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hojeda.call.dao.EmployeeRepository;
import com.hojeda.call.model.Employee;
import com.hojeda.call.model.Employee.State;
import com.hojeda.call.service.EmployeeService;
import com.hojeda.call.service.generic.GenericAbstractService;

@Service
public class EmployeeServiceImpl extends GenericAbstractService<EmployeeRepository, Employee, Long> implements EmployeeService{

	@Transactional(readOnly=true)
	public List<Employee> findByState(State state) {
		return getRepository().findByState(state);
	}
}
