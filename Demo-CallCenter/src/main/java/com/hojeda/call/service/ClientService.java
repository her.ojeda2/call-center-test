package com.hojeda.call.service;

import com.hojeda.call.model.Client;
import com.hojeda.call.service.generic.GenericService;

public interface ClientService extends GenericService<Client, Long>{

}
