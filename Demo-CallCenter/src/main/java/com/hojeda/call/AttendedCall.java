package com.hojeda.call;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.hojeda.call.dispatcher.Dispatcher;
import com.hojeda.call.model.Call;

/**
 * 
 * Representa una llamada atendida.
 * 
 * */
@Component(AttendedCall.BEAN_NAME)
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AttendedCall implements CallThread{

	private static final Logger logger = LoggerFactory.getLogger(AttendedCall.class);
	public static final String BEAN_NAME = "attendedCall";

	@Autowired
	private Dispatcher dispatcher; 
	
	private Call call;
	
	@Override
	public void run() {
		try {
			this.callWithEmployee();
		} catch (InterruptedException e) {
			logger.error("Error en la comunicación: ", e);
		}
	}

	private void callWithEmployee() throws InterruptedException {
		logger.info("Conectando la llamada...");
		Long callLength = callLength().longValue();
		logger.info("Llamada conectada.");
		Thread.sleep(callLength);
		logger.info("Llamada terminada. Duracción: " + (callLength/1000) + " Segundos.");
		call.setState(Call.State.ANSWERED);
		call.setCallLength(callLength);
		dispatcher.finalizeCall(this);
	}
	
	/**
	 * Devuelve un número aleatorio entre 5000 y 10000
	 * Este es el tiempo de duración de la llamada.
	 * */
	private Double callLength() {
		int min = 5000;
		int max = 10000;
		return Math.floor(Math.random()*(max-min)+min);
	}
	
	public Call getCall() {
		return call;
	}

	public void setCall(Call call) {
		this.call = call;
	}
	
}
