package com.hojeda.call;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.hojeda.call.dispatcher.Dispatcher;
import com.hojeda.call.model.Call;


/**
 * Representa las llamadas entrantes, internamente usa el {@link Dispatcher} para despachar las llamadas.
 * */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class IncomingCall implements CallThread {

	private static final Logger logger = LoggerFactory.getLogger(IncomingCall.class);
	public static final String BEAN_NAME = "incomingCall";
	
	@Autowired
	private Dispatcher dispatcher;
	
	private Call call;
	
	@Override
	public void run() {
		try {
			dispatcher.dispatchCall(this);
		} catch (InterruptedException e) {
			logger.error("Error al conectar la llamada: ", e);
		}
	}

	public Call getCall() {
		return call;
	}

	public void setCall(Call call) {
		this.call = call;
	}

}
