package com.hojeda.call.util;

import org.slf4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

public class ThreadPoolUtils {
	
	public static void waitForThreadsAndShutdown(ThreadPoolTaskExecutor taskExecutor, Long sleepTime, Logger logger) throws InterruptedException{
		waitForThreads(taskExecutor, sleepTime, logger);
		taskExecutor.shutdown();
	}

	public static void waitForThreads(ThreadPoolTaskExecutor taskExecutor, Long sleepTime, Logger logger)
			throws InterruptedException {
		for (;;) {
			int count = taskExecutor.getActiveCount();
			logger.info(logger.getName() + " -> Active Threads : " + count);
			Thread.sleep(sleepTime);
			if (count == 0) {
				Thread.sleep(sleepTime);
				break;
			}
		}
	}
	
	public static void waitForAvailableThread(ThreadPoolTaskExecutor taskExecutor, Long sleepTime, Logger logger) throws InterruptedException{
		for (;;) {
			int count = taskExecutor.getActiveCount();
			logger.info(logger.getName() + " -> Active Threads : " + count);
			if(count > 0) {
				if (count < taskExecutor.getCorePoolSize()) {
					break;
				} else {
					Thread.sleep(sleepTime);
				}
			} else break;
		}
	}
	
}
