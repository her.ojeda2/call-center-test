package com.hojeda.call.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hojeda.call.model.Call;

public interface CallRepository extends JpaRepository<Call, Long>{
	
	@Query("select count(call) from Call call where call.state = :state")
	public Long countCallByState(@Param("state")Call.State state);
	
	@Modifying
	@Query(value = "delete from Call c")
    public void truncate();
	
	public List<Call> findByState(Call.State state);
}
