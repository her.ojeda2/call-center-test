package com.hojeda.call.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hojeda.call.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

	public List<Employee> findByState(Employee.State state);
}
