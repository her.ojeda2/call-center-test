package com.hojeda.call.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hojeda.call.model.Client;

public interface ClientRepository extends JpaRepository<Client, Long>{

}
