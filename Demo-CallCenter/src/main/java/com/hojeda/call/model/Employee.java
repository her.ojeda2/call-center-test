package com.hojeda.call.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Employee implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5248288027671180739L;

	/**
	 * Tipos de empleados dentro del Call Center.
	 * El orden de este enumerado determina el orden de prioridad para la atención.
	 * @author Hernan D. Ojeda
	 * */
	public enum Type {OPERATOR, SUPERVISOR, DIRECTOR}
	
	/**
	 * Estados posibles de los empleados.
	 * @author Hernan D. Ojeda
	 * */
	public enum State {AVAILABLE, BUSY}
	
	@Id
	@GeneratedValue
	@Column(name="empĺoyee_id")
	private Long id;
	
	@Column(name="employee_name")
	private String name;
	
	@Column(name="employee_lastname")
	private String lastname;
	
	@Column(name="employee_state")
	@Enumerated(EnumType.STRING)
	private State state;
	
	@Column(name="employee_type")
	private Type type;
	
	@OneToMany(mappedBy="employee")
	private List<Call> calls;
	
	public Employee(String name, String lastname, State state, Type type) {
		this.name = name;
		this.lastname = lastname;
		this.state = state;
		this.type = type;
	}
	
	public Employee() {
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", lastname=" + lastname + ", state=" + state + ", type="
				+ type + "]";
	}
	
}
