package com.hojeda.call.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class Call implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7682536571614165365L;

	public enum State {ANSWERED, REJECTED}
	
	@Id
	@GeneratedValue
	@Column(name="call_id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="client_id")
	private Client client;
	
	@ManyToOne
	@JoinColumn(name="employee_id")
	private Employee employee;
	
	@Column(name="call_state")
	@Enumerated(EnumType.STRING)
	private State state;
	
	@Column(name="call_length")
	private Long callLength;

	@Transient
	private boolean clientDecideWait;
	
	public Call(Client client, Employee employee, boolean clientDecideWait) {
		this.client = client;
		this.employee = employee;
		this.clientDecideWait = clientDecideWait;
	}
	
	public Call(Client client, boolean clientDecideWait) {
		this.client = client;
		this.clientDecideWait = clientDecideWait;
	}
	
	public Call() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Long getCallLength() {
		return callLength;
	}

	public void setCallLength(Long callLength) {
		this.callLength = callLength;
	}
	
	public boolean isClientDecideWait() {
		return clientDecideWait;
	}

	public void setClientDecideWait(boolean clientDecideWait) {
		this.clientDecideWait = clientDecideWait;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Call other = (Call) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
