package com.hojeda.call.dispatcher;

import com.hojeda.call.AttendedCall;
import com.hojeda.call.IncomingCall;

public interface Dispatcher {

	/**
	 * <h1> Despacha todas las llamadas entrantes </h1>
	 * Su responsabilidad es ejecutar todas las llamadas entrantes.
	 * @author Hernan D. Ojeda
	 * */
	void dispatchCalls();
	
	/**
	 * <h1> Se encarga de cerrar llamadas atendidas </h1>
	 * @param call -> Llamada atendida que se va a cerrar.
	 * @author Hernan D. Ojeda
	 * */
	void finalizeCall(AttendedCall call);
	
	/**
	 * <h1> Despacha las llamadas entrantes </h1>
	 * @param incomingCall -> Llamada entrante a despachar.
	 * @author Hernan D. Ojeda
	 * */
	void dispatchCall(IncomingCall incomingCall) throws InterruptedException;
	
}
