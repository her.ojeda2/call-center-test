package com.hojeda.call.dispatcher;

import java.util.List;
import java.util.Optional;

import com.hojeda.call.model.Employee;

@FunctionalInterface
public interface DispatcherStrategy {

	public Optional<Employee> findAvailableEmployee(List<Employee> employees);
	
}
