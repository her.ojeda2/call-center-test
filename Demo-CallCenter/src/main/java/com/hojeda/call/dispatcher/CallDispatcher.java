package com.hojeda.call.dispatcher;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.hojeda.call.AttendedCall;
import com.hojeda.call.CallThread;
import com.hojeda.call.IncomingCall;
import com.hojeda.call.model.Call;
import com.hojeda.call.model.Employee;
import com.hojeda.call.model.Employee.State;
import com.hojeda.call.model.Employee.Type;
import com.hojeda.call.service.CallService;
import com.hojeda.call.service.EmployeeService;
import com.hojeda.call.util.ThreadPoolUtils;

/**
 * <h1> Despachante de llamadas </h1>
 * Se encarga de que se despachen todas las llamadas.
 * @author Hernan D. Ojeda
 * */
@Component
public class CallDispatcher implements Dispatcher{

	private static final Logger logger = LoggerFactory.getLogger(CallDispatcher.class);
	private static final Long THREADS_WAIT_TIME = 1000L;
	
	@Autowired
	private List<Call> incomingCalls;
	
	@Autowired
	private CallService callService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private ThreadPoolTaskExecutor incommingCallExecutor;
	
	@Autowired
	private ThreadPoolTaskExecutor attendedCallExecutor;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	@Qualifier("maxAttendCallsInProgress")
	private Integer maxAttendCallsInProgress;
	
	/**
	 * {@inheritDoc Dispatcher}
	 * <br>
	 * En el caso de que exitan llamadas rechazadas por no haber empleados disponibles, las vuelve a llamar.
	 * @author Hernan D. Ojeda
	 * */
	public void dispatchCalls() {
		attendedCallExecutor.initialize();
		try {
			if (incomingCalls.size() > 0) {
				this.handleIncomingCalls();
			} else {
				logger.info("No existen llamadas entrantes para atender.");
			}
			if (hasRejectedCalls()) {
				this.resumeRejectedCalls();
			} else {
				logger.info("No existen llamadas rechazadas para resolver.");
			}
		} catch (InterruptedException e) {
			logger.error("El proceso del Dispatcher ha fallado: ", e);
		} finally {
			try {
				ThreadPoolUtils.waitForThreadsAndShutdown(attendedCallExecutor, 1000L, logger);
			} catch (InterruptedException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	/**
	 * Despacha una llamada.
	 * <br>
	 * Si existe un empleado disponible para atenderla, conecta al cliente con ese empleado.
	 * <br>
	 * Si no existe, la rechaza.
	 * @author Hernan D. Ojeda
	 * */
	public synchronized void dispatchCall(IncomingCall incomingCall) throws InterruptedException {
		List<Employee> availableEmployees = employeeService.findByState(Employee.State.AVAILABLE);
		Optional<Employee> employee = this.getDefaultDispatcherStrategy().findAvailableEmployee(availableEmployees);
		if (employee.isPresent()) {
			incomingCall.getCall().setEmployee(employee.get());
			employee.get().setState(State.BUSY);
			employeeService.update(employee.get());
			ThreadPoolUtils.waitForAvailableThread(attendedCallExecutor, 1000L,logger);
			CallThread attendedCall = (CallThread) applicationContext.getBean(AttendedCall.BEAN_NAME);
			attendedCall.setCall(incomingCall.getCall());
			attendedCallExecutor.execute(attendedCall);
		} else {
			this.handleRejectedCall(incomingCall.getCall());
		}
	}
	
	/**
	 * Finaliza la llamada atendida.
	 * <br>
	 * Persite en la base de datos la llamada con si respectivo cliente, empleado que atendió y duración.
	 * <br>
	 * Cambia el estado del empleado a "Disponible"
	 * @author Hernan D. Ojeda
	 * */
	public synchronized void finalizeCall(AttendedCall attendedCall) {
		attendedCall.getCall().setState(com.hojeda.call.model.Call.State.ANSWERED);
		Call completedCall = callService.save(attendedCall.getCall());
		Employee employee = attendedCall.getCall().getEmployee();
		employee.setState(State.AVAILABLE);
		employeeService.update(employee);
		logger.info("La llamada Nro.: " + completedCall.getId() + ", ha finalizado satisfactoriamente.");
	}
	
	/**
	 * Estrategia utilizada para conectar con los empleados.
	 * <br>
	 * El orden de prioridad lo provee el enumerado de tipos de empleados.
	 * @see Type
	 * @author Hernan D. Ojeda
	 * */
	DispatcherStrategy getDefaultDispatcherStrategy() {
		return (availableEmployees) -> {
			Optional<Employee> employee = Optional.empty();
			for (Employee.Type type: Employee.Type.values()) {
				employee = availableEmployees.stream().filter(e -> e.getType().equals(type) && e.getState().equals(State.AVAILABLE)).findAny();
				if (employee.isPresent()) {
					break;
				} else {
					logger.info("No available " + type + " is found.");
				}
			}
			return employee;
		};
	}
	
	/**
	 * Se encarga de ejecutar las llamadas entrantes.
	 * <br>
	 * Toma las llamadas entrantes desde el contexto.
	 * @author Hernan D. Ojeda
	 */
	void handleIncomingCalls() throws InterruptedException {
		incommingCallExecutor.initialize();
		Boolean hasIncomingCalls = !incomingCalls.isEmpty();
		while (hasIncomingCalls) {
			ThreadPoolUtils.waitForAvailableThread(incommingCallExecutor, THREADS_WAIT_TIME, logger);
			Call call = incomingCalls.remove(0);
			CallThread inCall = (CallThread) applicationContext.getBean(IncomingCall.BEAN_NAME);
			inCall.setCall(call);
			incommingCallExecutor.execute(inCall);
			if (incomingCalls.isEmpty()) {
				ThreadPoolUtils.waitForThreads(incommingCallExecutor, THREADS_WAIT_TIME, logger);
			}
			hasIncomingCalls = !incomingCalls.isEmpty();
		}
		ThreadPoolUtils.waitForThreadsAndShutdown(incommingCallExecutor, THREADS_WAIT_TIME, logger);
	}

	/**
	 *  Maneja las llamadas rechazadas dependiendo de la decisión del cliente.
	 *  <br>
	 *  Si el cliente decide esperar, va nuevamente a la cola.
	 *  <br>
	 *  Sino se guarda la llamada rechazada.
	 *  @author Hernan D. Ojeda
	 * */
	void handleRejectedCall(Call call) {
		if (call.isClientDecideWait()) {
			incomingCalls.add(0, call);
		} else {
			call.setState(Call.State.REJECTED);
			callService.save(call);
		}
	}
	
	/**
	 * Levanta todas las llamadas rechazadas de la base de datos y vuelve a llamar a los clientes.
	 * <br>
	 * Se asegura de que nunca queden llamadas rechazadas.
	 * @author Hernan D. Ojeda
	 * */
	void resumeRejectedCalls() {
		List<Call> rejectedCalls = callService.findByState(Call.State.REJECTED);
		while (!rejectedCalls.isEmpty()) {
			rejectedCalls.forEach(call -> incomingCalls.add(call));
			incommingCallExecutor.setCorePoolSize(maxAttendCallsInProgress);
			try {
				this.handleIncomingCalls();
			} catch (InterruptedException e) {
				logger.error("El proceso del Dispatcher ha fallado: ", e);
			}
			rejectedCalls = callService.findByState(Call.State.REJECTED);
		}
	}

	/**
	 * Evalúa si existen llamdas rechazadas.
	 * @author Hernan D. Ojeda
	 * */
	private boolean hasRejectedCalls() {
		return callService.existCallByState(Call.State.REJECTED);
	}
	
}
