package com.hojeda.call;

import com.hojeda.call.model.Call;

/**
 * Interfaz de la cual extienden todas las llamadas entrantes o atendidas.
 * 
 * */
public interface CallThread extends Runnable{

	public Call getCall();
	public void setCall(Call call);
	
}
