package com.hojeda.call.config;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import com.hojeda.call.model.Call;

@Configuration
@ComponentScan("com.hojeda")
@PropertySource(value="app.properties", ignoreResourceNotFound=true)
@Import({JpaConfiguration.class, ThreadPoolConfiguration.class})
public class AppConfiguration {

	@Bean
	public List<Call> incomingCalls() {
		return new CopyOnWriteArrayList<Call>();
	}
	
}
