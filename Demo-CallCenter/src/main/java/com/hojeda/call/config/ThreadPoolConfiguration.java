package com.hojeda.call.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.hojeda.call.dao.EmployeeRepository;

@Configuration
public class ThreadPoolConfiguration {

	public static final Integer MAX_POOL_SIZE = 20;
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Bean
	public ThreadPoolTaskExecutor incommingCallExecutor() {
		ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
		pool.setCorePoolSize(MAX_POOL_SIZE);
		pool.setWaitForTasksToCompleteOnShutdown(true);
		return pool;
	}
	
	@Bean
	public ThreadPoolTaskExecutor attendedCallExecutor() {
		ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
		pool.setCorePoolSize(maxAttendCallsInProgress());
		pool.setWaitForTasksToCompleteOnShutdown(true);
		return pool;
	}
	
	@Bean
	public Integer maxAttendCallsInProgress() {
		return ((Long)employeeRepository.count()).intValue();
	}
}
