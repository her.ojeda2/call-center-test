package com.hojeda.call.config;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import com.hojeda.call.model.Call;
import com.hojeda.call.model.Client;
import com.hojeda.call.model.Employee;
import com.hojeda.call.model.Employee.State;
import com.hojeda.call.model.Employee.Type;
import com.hojeda.call.service.ClientService;
import com.hojeda.call.service.EmployeeService;

@Configuration
@ComponentScan("com.hojeda")
@PropertySource("app-test.properties")
@Import({JpaConfiguration.class, ThreadPoolConfiguration.class})
public class TestConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(TestConfiguration.class);
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private ClientService clientService;
	
	@PostConstruct
	public void initializeData() {
		logger.info("Inicializando datos para test...");
		employeeService.saveAll(this.generateEmployees());
		clientService.saveAll(this.generateClients());
	}
	
	/**
	 * <h1> Collección de llamadas entrantes </h1>
	 * Tener este bean diferenciado entre la configuración de Test y la configuración principal
	 * permite probar con diferentes tipos de colecciones para evaluar rendimiento.
	 * @return -> Thread Safe Collection
	 **/
	@Bean
	public List<Call> incomingCalls() {
		return new CopyOnWriteArrayList<Call>();
	}
	
	private List<Employee> generateEmployees() {
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee("Julio", "Garin", State.AVAILABLE, Type.OPERATOR));
		employees.add(new Employee("Roberto", "Skripka", State.AVAILABLE, Type.OPERATOR));
		employees.add(new Employee("Jose", "Gonzales", State.AVAILABLE, Type.OPERATOR));
		employees.add(new Employee("Amalia", "Fernandez", State.AVAILABLE, Type.OPERATOR));
		employees.add(new Employee("Florencia", "Bularte", State.AVAILABLE, Type.OPERATOR));
		employees.add(new Employee("Gaston", "Ortega", State.AVAILABLE, Type.OPERATOR));
		employees.add(new Employee("Edgardo", "Ritacco", State.AVAILABLE, Type.OPERATOR));
		employees.add(new Employee("Pablo", "Leghero", State.AVAILABLE, Type.SUPERVISOR));
		employees.add(new Employee("Gustavo", "De Martino", State.AVAILABLE, Type.SUPERVISOR));
		employees.add(new Employee("Eduardo", "De Santis", State.AVAILABLE, Type.DIRECTOR));
		return employees ;
	}
	
	private List<Client> generateClients() {
		List<Client> clients = new ArrayList<Client>();
		clients.add(new Client("Armando Sanchez", 123456L));
		clients.add(new Client("Fernando Rocha", 565567L));
		clients.add(new Client("Humberto Rodriguez", 5467578L));
		clients.add(new Client("Natalia Santillan", 56565467L));
		clients.add(new Client("Romina Martinez", 565675789L));
		clients.add(new Client("Barbara Jimenez", 798956745L));
		clients.add(new Client("Lidia Spósito", 567675786L));
		clients.add(new Client("Raul Scala", 47686745L));
		clients.add(new Client("Gabriel Frias", 68785534L));
		clients.add(new Client("Jimena Garay", 35356687L));
		clients.add(new Client("Fabio Grossi", 6787455234L));
		clients.add(new Client("Ezequiel Lastra", 345356796L));
		clients.add(new Client("Sebastian Capotondo", 798956745L));
		clients.add(new Client("Micaela Galeazzi", 6877987435L));
		clients.add(new Client("Daniel Galante", 6878456343L));
		return clients ;
	}
	
}
