package com.hojeda.call;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.*;

import java.util.List;
import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hojeda.call.config.TestConfiguration;
import com.hojeda.call.dispatcher.CallDispatcher;
import com.hojeda.call.model.Call;
import com.hojeda.call.model.Call.State;
import com.hojeda.call.model.Client;
import com.hojeda.call.service.CallService;
import com.hojeda.call.service.ClientService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={TestConfiguration.class})
public class CallTest {

	@Autowired
	private List<Call> incomingCalls;
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private CallDispatcher callDispatcher;
	
	@Autowired
	private CallService callService; 
	
	/**
	 * Se ejecutan 10 llamadas y se prueba el funcionamiento del Proceso.
	 * <br>
	 * En este caso nunca llega al límite de empleados disponibles, 
	 * entonces no presenta mayor dificultad.
	 * */
	@Test
	public void manage10Calls() {
		List<Client> clients = clientService.findAllLimitResult(10);
		clients.forEach(client -> incomingCalls.add(new Call(client, true)));
		callDispatcher.dispatchCalls();
		assertThat(incomingCalls.isEmpty(), is(true));
		assertThat(callService.findByState(State.REJECTED).isEmpty(), is(true));
	}
	
	/**
	 * Se ejecutan mas de 10 llamadas, en donde todos los clientes deciden esperar para ser atendidos.
	 * <br>
	 * En este caso no se rechaza ninguna llamada, cada vez que no haya un empleado disponible, 
	 * se vuelve a poner al cliente en espera.
	 * <br>
	 * A medida que se vayan desocupando los empleados van a atender una a una todas las llamdas.
	 * */
	@Test
	public void moreThan10CallsWithAllDecideTrue() {
		List<Client> clients = clientService.findAll();
		clients.forEach(client -> incomingCalls.add(new Call(client, true)));
		callDispatcher.dispatchCalls();
		assertThat(incomingCalls.isEmpty(), is(true));
		assertThat(callService.findByState(State.REJECTED).isEmpty(), is(true));
	}
	
	/**
	 * Se ejecutan mas de 10 llamadas, en donde todos los clientes deciden que su llamada sea rechazada.
	 * <br>
	 * En este caso, luego de que se desocupen todos los empleados, 
	 * se van a volver a llamar a todos los clientes cuyas llamadas fueron rechazadas.
	 * <br>
	 * De esta manera nunca queda ningún cliente sin atender.
	 * */
	@Test
	public void moreThan10CallsWithAllDecideFalse() {
		List<Client> clients = clientService.findAll();
		clients.forEach(client -> incomingCalls.add(new Call(client, false)));
		callDispatcher.dispatchCalls();
		assertThat(incomingCalls.isEmpty(), is(true));
		assertThat(callService.findByState(State.REJECTED).isEmpty(), is(true));
	}
	
	/**
	 * Se ejecutan mas de 10 llamadas, en donde la mitad de los clientes 
	 * deciden que su llamada sea rechazada y la otra mitad deciden esperar.
	 * <br>
	 * Este caso es una combinación en la toma de decisiones de los clientes. 
	 * Si el cliente decidió esperar y no encuentra empleados disponibles entonces va a seguir quedando en espera.
	 * Si el cliente no quiere esperar, lo que va a pasar es que su llamada se de por rechazada 
	 * para que luego se comuniquen desde el call center.
	 * <br>
	 * De esta manera se satisfacen las necesidades de las personas que no quieren cortar hasta ser atendidas
	 * y de las personas que no quieren esperar, entonces van a ser llamadas una vez que se desocupen todos los empleados.
	 * */
	@Test
	public void moreThan10CallsWithMixedDecisions() {
		List<Client> clients = clientService.findAll();
		IntStream.range(0, clients.size())
			.forEach(idx -> incomingCalls.add(new Call(clients.get(idx), idx%2==0)));
		callDispatcher.dispatchCalls();
		assertThat(incomingCalls.isEmpty(), is(true));
		assertThat(callService.findByState(State.REJECTED).isEmpty(), is(true));
	}
	
}
