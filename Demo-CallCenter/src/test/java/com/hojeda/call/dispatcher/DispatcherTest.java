package com.hojeda.call.dispatcher;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hojeda.call.AttendedCall;
import com.hojeda.call.config.TestConfiguration;
import com.hojeda.call.model.Call;
import com.hojeda.call.model.Client;
import com.hojeda.call.model.Employee;
import com.hojeda.call.model.Employee.State;
import com.hojeda.call.model.Employee.Type;
import com.hojeda.call.service.CallService;
import com.hojeda.call.service.ClientService;
import com.hojeda.call.service.EmployeeService;
import com.hojeda.call.util.ThreadPoolUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={TestConfiguration.class})
public class DispatcherTest {

	private static final Logger logger = LoggerFactory.getLogger(DispatcherTest.class);
	
	@Autowired
	private CallDispatcher dispatcher;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private CallService callService; 
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private List<Call> incomingCalls;
	
	@Autowired
	private ThreadPoolTaskExecutor attendedCallExecutor;
	
	@Test
	public void dispatcherStrategy_whenFoundOperatorAvailable_thenReturnOperator() {
		List<Employee> employees = employeeService.findAll();
		employees.forEach(emp -> emp.setState(State.AVAILABLE));
		Optional<Employee> selectedEmployee = dispatcher.getDefaultDispatcherStrategy().findAvailableEmployee(employees);
		assertThat(Type.OPERATOR.equals(selectedEmployee.get().getType()), is(true));
	}
	
	@Test
	public void dispatcherStrategy_whenAllOperatorsAreBusy_thenReturnSupervisor() {
		List<Employee> employees = employeeService.findAll();
		employees.stream().filter(emp -> emp.getType().equals(Type.OPERATOR)).forEach(emp -> emp.setState(State.BUSY));
		Optional<Employee> selectedEmployee = dispatcher.getDefaultDispatcherStrategy().findAvailableEmployee(employees);
		assertThat(Type.SUPERVISOR.equals(selectedEmployee.get().getType()), is(true));
	}
	
	@Test
	public void dispatcherStrategy_whenOnlyDirectorIsAvailable_thenReturnDirector() {
		List<Employee> employees = employeeService.findAll();
		employees.stream().filter(emp -> emp.getType().equals(Type.OPERATOR) || emp.getType().equals(Type.SUPERVISOR))
			.forEach(emp -> emp.setState(State.BUSY));
		Optional<Employee> selectedEmployee = dispatcher.getDefaultDispatcherStrategy().findAvailableEmployee(employees);
		assertThat(Type.DIRECTOR.equals(selectedEmployee.get().getType()), is(true));
	}
	
	@Test
	public void whenFinalizeCall_thenEmployeeBecomesAvailableAndPersistAttendedCall() {
		callService.truncateCalls();
		incomingCalls.clear();
		AttendedCall attendedCall = new AttendedCall();
		Client client = clientService.findAllLimitResult(1).stream().findFirst().get();
		Employee employee = employeeService.findAllLimitResult(1).stream().findFirst().get();
		employee.setState(State.BUSY);
		employee = employeeService.update(employee);
		Call call = new Call(client, employee, true);
		attendedCall.setCall(call);
		
		assertThat(callService.existCallByState(com.hojeda.call.model.Call.State.ANSWERED), is(false));
		assertThat(employee.getState().equals(State.BUSY), is(true));
		
		dispatcher.finalizeCall(attendedCall);
		employee = employeeService.findById(employee.getId()).get();
		call = callService.findByState(com.hojeda.call.model.Call.State.ANSWERED).stream().findFirst().get();
		
		assertThat(call.getState().equals(com.hojeda.call.model.Call.State.ANSWERED), is(true));
		assertThat(employee.getState().equals(State.AVAILABLE), is(true));
	}
	
	@Test
	public void testIncomingCallsHandler() throws InterruptedException {
		attendedCallExecutor.initialize();
		callService.truncateCalls();
		incomingCalls.clear();
		List<Client> clients = clientService.findAllLimitResult(5);
		clients.forEach(client -> incomingCalls.add(new Call(client, true)));
		
		assertThat(callService.count() == 0L, is(true));
		
		dispatcher.handleIncomingCalls();
		ThreadPoolUtils.waitForThreadsAndShutdown(attendedCallExecutor, 1000L, logger);
		System.out.println("Cantidad de llamadas: " + callService.count());
		
		assertThat(callService.count() == clients.size(), is(true));
	}
	
	@Test
	public void testRejectedCallsHandler() {
		callService.truncateCalls();
		incomingCalls.clear();
		List<Client> clients = clientService.findAllLimitResult(5);
		
		assertThat(incomingCalls.size() == 0, is(true));
		assertThat(callService.count() == 0, is(true));
		
		IntStream.range(0, clients.size())
			.forEach(idx -> dispatcher.handleRejectedCall(new Call(clients.get(idx), idx%2==0)));
		
		assertThat(incomingCalls.size() != 0, is(true));
		assertThat(callService.existCallByState(com.hojeda.call.model.Call.State.REJECTED), is(true));
	}
	
	@Test
	public void whenHasRejectedCalls_thenResumeRejectedCalls() throws InterruptedException {
		attendedCallExecutor.initialize();
		callService.truncateCalls();
		incomingCalls.clear();
		List<Client> clients = clientService.findAllLimitResult(5);
		clients.forEach(client -> dispatcher.handleRejectedCall(new Call(client, false)));
		
		callService.findAll().forEach(call -> assertThat(call.getState().equals(com.hojeda.call.model.Call.State.REJECTED), is(true)));
		
		dispatcher.resumeRejectedCalls();
		ThreadPoolUtils.waitForThreadsAndShutdown(attendedCallExecutor, 1000L, logger);
		
		callService.findAll().forEach(call -> assertThat(call.getState().equals(com.hojeda.call.model.Call.State.ANSWERED), is(true)));
	}
	
}
